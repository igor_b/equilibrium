## Info

Equilibrium is generative art installation. More info at [https://hyperglitch.com/articles/equilibrium](https://hyperglitch.com/articles/equilibrium)


## Installation

```
git clone https://igor_b@bitbucket.org/igor_b/equilibrium.git
cd equilibrium
git submodule update --init --recursive
```


