var myrng = null;
var w = null;
var h = null;

var stats = {w: w*h, b: 0};
var step = 0;
var start_time = 0;
var seed = 0;

var is_paused = false;
var is_local = false;

var timeout = 5;

function pause(){
  is_paused = true;
  document.getElementById('pause').style.display = 'block';
}

function unpause(){
  is_paused = false;
  document.getElementById('pause').style.display = 'none';
}


function initRng(){
  myrng = new Math.seedrandom(seed);
}

function getCtx(){
  let canvas = document.getElementById('c');
  return canvas.getContext("2d");
}

function startPainter(reset){
  let canvas = document.getElementById('c');
  let sd = parseInt(window.location.search.substr(1), 16);
  if(isNaN(sd) || reset){
    sd = parseInt(Math.random()*65536);
    seed = ('0000'+sd.toString(16)).substr(-4);
    window.location.search = seed;
    return;
  }
  seed = sd.toString(16);
  initRng();

  w = canvas.width;
  h = canvas.height;
  start_time = new Date();

  ctx = getCtx();
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, w, h);
  calcStats();
  paintStep();
};


function calcStats(){
  let ctx = getCtx();
  let d = ctx.getImageData(0, 0, w, h);
  let s = {w: 0, b: 0};
  for(let i=0; i<d.data.length; i+=4){
    if(d.data[i]==0) s.b++;
    else s.w++;
  }
  stats = s;
}

function printStats(tm){
  document.getElementById('el_seed').innerText = seed;
  document.getElementById('el_white').innerText = stats.w+' ('+(stats.w/(w*h)*100).toFixed(3)+'%)';
  document.getElementById('el_black').innerText = stats.b+' ('+(stats.b/(w*h)*100).toFixed(3)+'%)';
  document.getElementById('el_steps').innerText = step;
}

function randint(range){
  return Math.abs(myrng.int32()%parseInt(range));
}


/**
 * Polar Form of the Box-Muller Transform for normally distributed PRNG generation.
 * See the following for a perfomance comparison between Basic and Polar forms:
 *
 *     http://jsperf.com/box-mullerperformance
 *
 * source: https://jsfiddle.net/ssell/qzzvruc4/
 */
function gaussianRand(){
  this.generate = true;
  this.value0   = 0.0;
  this.value1   = 0.0;
  if(this.generate){
    var x1 = 0.0;
    var x2 = 0.0;
    var w  = 0.0;
    do{
      x1 = (2.0 * myrng.double()) - 1.0;
      x2 = (2.0 * myrng.double()) - 1.0;
      w  = (x1 * x1) + (x2 * x2);
    } while(w >= 1.0);
    w = Math.sqrt((-2.0 * Math.log(w)) / w);
    this.value0 = x1 * w;
    this.value1 = x2 * w;
    result = this.value0;
  }
  else{
    result = this.value1;
  }
  this.generate = !this.generate
  return result;
}


function gaussianRandAdj(mean, stddev) {
  const value = gaussianRand();
  return ((value * stddev) + mean);
}


function drawRandomPoints(ctx){
  const npoints = randint(100);
  const x = randint(w);
  const y = randint(h);
  for(let i=0; i<npoints; i++){
    ctx.fillRect(gaussianRandAdj(x, w/20), gaussianRandAdj(y, w/20), 1, 1);
  }
}

function drawRandomRect(ctx){
  const x = randint(w);
  const y = randint(h);
  ctx.fillRect(x, y, randint(h/4), randint(h/4));
}

function drawRandomCircle(ctx){
  ctx.beginPath();
  const x = randint(w);
  const y = randint(h);
  const r = randint(h/6);
  ctx.arc(x, y, r, 0, 2 * Math.PI);
  ctx.fill();
}

function drawRandomLine(ctx){
  const x = randint(w);
  const y = randint(h);
  const x2 = randint(w);
  const y2 = randint(h);
  const lw = randint(20)+1;
  ctx.lineWidth = lw;
  ctx.beginPath();
  ctx.moveTo(x, y);
  ctx.lineTo(x2, y2);
  ctx.stroke();
}

function drawBezier(ctx){
  const nseg = randint(3)+2;
  let sx = randint(w);
  let sy = randint(h);
  let x = sx;
  let y = sy;
  const _cp2x = randint(w);
  const _cp2y = randint(h);
  let cp1x;
  let cp1y;
  let cp2x = _cp2x;
  let cp2y = _cp2y;
  ctx.beginPath();
  for(let i=0; i<nseg; i++){
    ctx.moveTo(x, y);
    cp1x = 2*x-cp2x;
    cp1y = 2*y-cp2y;
    cp2x = randint(w);
    cp2y = randint(h);
    let xe = randint(w);
    let ye = randint(h);
    if(i==nseg-1){
      xe = sx;
      ye = sy;
      cp2x = _cp2x;
      cp2y = _cp2y;
    }
    ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, xe, ye);
    x = xe;
    y=ye;
  }
  ctx.fill();
}


function paintStep(){
  if(is_paused){
    setTimeout(paintStep, timeout);
    return;
  }

  calcStats();
  let ctx = getCtx();
  let col = stats.w>stats.b?'black':'white';
  ctx.fillStyle = col;
  ctx.strokeStyle = col;

  const rn = randint(100);
  if(rn<10){
    drawBezier(ctx);
  }
  else if(rn<30){
    drawRandomRect(ctx);
  }
  else if(rn<50){
    drawRandomPoints(ctx);
  }
  else if(rn<70){
    drawRandomCircle(ctx);
  }
  else{
    drawRandomLine(ctx);
  }
  /*
  switch(randint(5)){
    case 0:
      drawRandomLine(ctx);
      break;
    case 1:
      drawRandomRect(ctx);
      break;
    case 2:
      drawRandomCircle(ctx);
      break;
    case 3:
      drawRandomPoints(ctx);
      break;
    case 4:
      drawBezier(ctx);
      break;
  }
  */

  step++;

  calcStats();
  printStats();
  if(stats.w==stats.b){
    document.getElementById('el_eq').innerText = 'YES!';
    printStats(true);
    if((((new Date())-start_time)/1000.0 > 60*60) || (step<30)){
      gutenberg();
    }
    triggerReset(15);
    return;
  }

  setTimeout(paintStep, timeout);
}


function triggerReset(v){
  if(v==0){
    startPainter(true);
    return;
  }
  document.getElementById('reset').textContent = 'Reset (in '+v+'s)';
  setTimeout(function(){triggerReset(v-1)}, 1000);
}


function gutenberg(){
  if(!is_local) return;
  pause();
  c.toBlob(function(blob) {
    fetch('/printer.php?seed='+seed+'&w='+stats.w+'&b='+stats.b+'&step='+step, {
      method: 'POST',
      body: blob
    })
      .then(function(response){
        unpause();
      })
      .then(function(result){
        unpause();
      })
  }, 'image/png');
}


