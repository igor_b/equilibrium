<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . DIRECTORY_SEPARATOR . "phpqrcode" . DIRECTORY_SEPARATOR . "qrlib.php";

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\CapabilityProfile;

$fp = fopen('/tmp/print.png', 'wb');
fwrite($fp, file_get_contents('php://input'));
fclose($fp);

$url = 'https://eq.hglt.ch/?'.$_GET['seed'];
QRcode::png($url, '/tmp/qr.png', 'M', 6);
// center it on canvas
system("/usr/bin/convert /tmp/qr.png -gravity center -extent 400x185 /tmp/qr_e.png");
try {
    $profile = CapabilityProfile::load("POS-5890");

    /* Fill in your own connector here */
    $connector = new FilePrintConnector("/dev/usb/lp0");

    /* Start the printer */
    $logo = EscposImage::load("/tmp/print.png", false);
    $qr = EscposImage::load("/tmp/qr_e.png", false);
    $printer = new Printer($connector, $profile);

    $printer->feed(1);

    $printer->setJustification(Printer::JUSTIFY_CENTER);
    $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $printer->text("Equilibrium\n");
    $printer->selectPrintMode();
    $printer->text("by Igor Brkić\n");

    $printer->feed(2);

    if ($profile->getSupportsGraphics()) {
        $printer->graphics($logo);
    }
    if ($profile->getSupportsBitImageRaster() && !$profile->getSupportsGraphics()) {
        $printer->bitImage($logo);
    }

    $printer->feed(1);

    $printer->setJustification(Printer::JUSTIFY_CENTER);
    if ($profile->getSupportsGraphics()) {
        $printer->graphics($qr);
    }
    if ($profile->getSupportsBitImageRaster() && !$profile->getSupportsGraphics()) {
        $printer->bitImage($qr);
    }
    $printer->text($url);

    /* Footer */
    $printer->feed(2);
    $printer->text("Drawing Machines by Radiona.org\n");
    $printer->feed(4);

    /* Cut the receipt and open the cash drawer */
    $printer->cut();
    $printer->pulse();
    $printer->close();

} catch (Exception $e) {
    echo $e->getMessage();
} finally {
    $printer->close();
}

